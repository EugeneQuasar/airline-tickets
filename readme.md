# Flexberry

**Реализовано:**

- Верстка резиново-адаптивной страницы сайта до 400px.
- Получение идентификатора запроса, затем рекурсивная отправка запросов к серверу до тех пор, пока не будет получен полный массив данных, не зависимо от ошибок сервера.
- сохранение данных с сервера в локальную переменную, для скорости и удобства работы.
- 3 кнопки сортировки и отображения билетов по цене, скорости и оптимальный ( сортирует массив всех билетов относительно цены и времени перелёта ) .
- отрисовка только 5 билетов с читаемым форматом времени и картинки авиакомпании.
- фильтрация по селектору "Без пересадок" ( снимает галочки с других селекторов и фильтрует список по 0 пересадок в любую из сторон ).
- показ всех билетов через селектор "Все" ( ставит галочки на все селекторы и отображает все полученные с базы тикери по сортировке "самый дешевый" ).
- мини анимацию загрузки данных.

( с учетом, что это тестовое задание и оно не может быть отправлено на продакшн, доработал некоторые необязательные фичи )
