let tikersDiv = document.querySelector(".content-tikers");
let byPriceBtn = document.querySelector(".sortByPrice");
let bySpeedBtn = document.querySelector(".sortBySpeed");
let byOptimalBtn = document.querySelector(".sortByOptimal");
let transferAllInpt = document.getElementById("transfer-all");
let transferNoneInput = document.getElementById("transfer-none");
let transferNoneSelectors = document.querySelectorAll(".transfer-none");
let transferAllBtn = document.querySelectorAll(".transfer-all");
let allTransferBtns = document.querySelectorAll(".select-box_input");

let arrAllTikers = [];
let arrSortingBySelect = [];
let arrSortingByTransfer = [];
let fulFilledObj = false;

byPriceBtn.addEventListener("click", sortByPrice);
bySpeedBtn.addEventListener("click", sortBySpeed);
byOptimalBtn.addEventListener("click", sortByOptimal);
transferAllBtn.forEach((el) => {
  el.addEventListener("click", allTransfer);
});
transferNoneSelectors.forEach((el) => {
  el.addEventListener("click", noTransfer);
});
document.addEventListener("DOMContentLoaded", getTikersList);

function getTikersList() {
  fetch("https://front-test.beta.aviasales.ru/search")
    .then((response) => response.json())
    .then((res) => sendFetch(res.searchId));
}

function sendFetch(id) {
  fetch(`https://front-test.beta.aviasales.ru/tickets?searchId=${id}`)
    .then((response) => response.json())
    .then((res) => {
      fulFilledObj = res.stop;
      arrAllTikers = arrAllTikers.concat(res.tickets);
      if (!res.stop) {
        sendFetch(id);
      } else {
        sortByPrice();
      }
    })
    .catch((e) => {
      if (!fulFilledObj) {
        sendFetch(id);
      } else {
        sortByPrice();
      }
    });
}

function sortByPrice(event) {
  if (arrAllTikers.length === 0) return;
  let arr = [...arrAllTikers];
  arrSortingBySelect = arr.sort((a, b) => {
    if (a.price > b.price) return 1;
    if (a.price < b.price) return -1;
    if (a.price == b.price) return 0;
  });
  fullTikers(arrSortingBySelect);
  toggleClass(event || byPriceBtn);
}

function sortBySpeed(event) {
  if (arrSortingBySelect.length === 0) return;
  let arr = [...arrAllTikers];
  arrSortingBySelect = arr.sort((a, b) => {
    let minA = Math.min(a.segments[0].duration, a.segments[1].duration);
    let minB = Math.min(b.segments[0].duration, b.segments[1].duration);
    if (minA > minB) return 1;
    if (minA < minB) return -1;
    if (minA == minB) return 0;
  });
  fullTikers(arrSortingBySelect);
  toggleClass(event);
}

function sortByOptimal(event) {
  if (arrSortingBySelect.length === 0) return;
  let arr = [...arrAllTikers];
  arrSortingBySelect = arr.sort((a, b) => {
    let minA = Math.min(a.segments[0].duration, a.segments[1].duration);
    let minB = Math.min(b.segments[0].duration, b.segments[1].duration);
    if (minA > minB && a.price > b.price) {
      return 1;
    }
    if (minA < minB && a.price < b.price) return -1;
    if (minA == minB) return 0;
  });
  fullTikers(arrSortingBySelect);
  toggleClass(event);
}

function sortingByNoTransfer() {
  if (arrSortingBySelect.length === 0) return;
  let arr = [...arrSortingBySelect];
  let res = arr.filter((el) => {
    if (el.segments[0].stops.length == 0) {
      return true;
    }
  });
  arrSortingByTransfer = res;
  fullTikers(arrSortingByTransfer);
}
function allTransfer(e) {
  e.preventDefault();
  if (!transferAllInpt.checked) {
    allTransferBtns.forEach((el) => {
      el.checked = true;
    });
    sortByPrice();
  } else {
    transferAllInpt.checked = false;
  }
}

function noTransfer(e) {
  e.preventDefault();
  if (!transferNoneInput.checked) {
    allTransferBtns.forEach((el) => {
      el.checked = false;
    });
    transferNoneInput.checked = true;
    sortingByNoTransfer();
  } else {
    transferNoneInput.checked = false;
  }
}

function getAirTime(obj) {
  let x = obj.date.split("T")[1].split(":");
  let y = obj.duration;
  let hour = +x[0] + Math.floor(y / 60);
  let min = +x[1] + (y % 60);
  if (min >= 60) {
    hour++;
    min = min - 60;
  }
  if (hour > 24) {
    hour -= 24;
  }
  return `${x[0]}:${x[1]} - ${hour}:${min}`;
}

function getPriceStr(num) {
  let res = "";
  let arr = [...(num + "")];
  for (let i = arr.length - 1; i >= 0; i--) {
    res = arr[i] + res;
    if ((arr.length - i) % 3 == 0) {
      res = " " + res;
    }
  }
  return res;
}

function toggleClass(e) {
  [byPriceBtn, bySpeedBtn, byOptimalBtn].forEach((el) =>
    el.classList.remove("content__sort_btn-active")
  );
  (e.target || e).classList.add("content__sort_btn-active");
}

function fullTikers(arr) {
  arr = arr.splice(0, 5);
  tikersDiv.innerHTML = "";
  arr.forEach((el) => {
    tikersDiv.innerHTML += `
    <div class="tiker-wrap">
      <div class="tiker__head">
        <div class="tiker__head_price">${getPriceStr(el.price)} P</div>
        <div class="tiker__head_iata">
          <img src="http://pics.avs.io/99/36/${el.carrier}.png" alt="AirLine" />
        </div>
      </div>
      <div class="tiker__state">
        <div>
          <div class="tiker__text-gray">${el.segments[0].origin} - ${
      el.segments[0].destination
    }</div>
          <div class="tiker__text-black">${getAirTime(el.segments[0])}</div>
        </div>
        <div>
          <div class="tiker__text-gray">В Пути</div>
          <div class="tiker__text-black">${Math.floor(
            el.segments[0].duration / 60
          )}ч ${el.segments[0].duration % 60}м</div>
        </div>
        <div>
          <div class="tiker__text-gray">${
            el.segments[0].stops.length
          } ПЕРЕСАДК${el.segments[0].stops.length == 1 ? "А" : "И"}</div>
          <div class="tiker__text-black">${
            el.segments[0].stops.length > 0
              ? el.segments[0].stops.join(", ")
              : ""
          }</div>
        </div>
      </div>
      <div class="tiker__state">
        <div>
          <div class="tiker__text-gray">${el.segments[1].origin} - ${
      el.segments[1].destination
    }</div>
          <div class="tiker__text-black">${getAirTime(el.segments[1])}</div>
        </div>
        <div>
          <div class="tiker__text-gray">В Пути</div>
          <div class="tiker__text-black">${Math.floor(
            el.segments[1].duration / 60
          )}ч ${el.segments[1].duration % 60}м</div>
        </div>
        <div>
          <div class="tiker__text-gray">${
            el.segments[1].stops.length
          } ПЕРЕСАДК${el.segments[1].stops.length == 1 ? "А" : "И"}</div>
          <div class="tiker__text-black">${
            el.segments[1].stops.length > 0
              ? el.segments[1].stops.join(", ")
              : ""
          }</div>
        </div>
      </div>
    </div>`;
  });
  tikersDiv.innerHTML += `<div class="show-more-tikers">Показать еще 5 билетов!</div>`;
}
